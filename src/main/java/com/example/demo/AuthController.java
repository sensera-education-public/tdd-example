package com.example.demo;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

@RestController("auth")
public class AuthController {
    AuthService authService;

    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    public ResponseEntity<String> login(String userName, String password) {
        try {
            return ResponseEntity.ok(authService.login(userName, password));
        } catch (AuthenticationFailedException e) {
            return ResponseEntity
                    .status(401)
                    .build();
        }
    }
}
