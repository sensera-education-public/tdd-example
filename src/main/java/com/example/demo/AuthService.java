package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class AuthService {
    TokenRepository tokenRepository;
    UserRepository userRepository;
    PasswordVerifier passwordVerifier;

    public AuthService(TokenRepository tokenRepository, UserRepository userRepository, PasswordVerifier passwordVerifier) {
        this.tokenRepository = tokenRepository;
        this.userRepository = userRepository;
        this.passwordVerifier = passwordVerifier;
    }

    public String login(String userName, String password) throws AuthenticationFailedException {
        return userRepository.findUserByUserName(userName)
                .filter(user -> passwordVerifier.verifyPassword(password, user.getEncryptedPassword()))
                .map(user -> tokenRepository.createToken())
                .orElseThrow(AuthenticationFailedException::new);
    }
}
