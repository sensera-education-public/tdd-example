package com.example.demo;

import lombok.Value;

@Value
public class User {
    String userName;
    byte[] encryptedPassword;
}
