package com.example.demo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SpringBootTest
public class AuthControllerTest {

    @Autowired AuthController authController;
    @MockBean AuthService authService;
    String userToken;

    @BeforeEach
    void setUp() {
        userToken = UUID.randomUUID().toString();
    }

    @Test
    void test_login_success() throws AuthenticationFailedException {
        // Given
        when(authService.login(anyString(),anyString())).thenReturn(userToken);

        // When
        ResponseEntity<String> responseEntity =  authController.login("Arne","lösen");

        // Then
        assertEquals(responseEntity.getStatusCodeValue(), 200);
        assertEquals(userToken, responseEntity.getBody());
    }

    @Test
    void test_login_fail() throws AuthenticationFailedException {
        // Given
        when(authService.login(anyString(),anyString())).thenThrow(new AuthenticationFailedException());

        // When
        ResponseEntity<String> responseEntity = authController.login("Arne", "lösen");

        // Then
        assertEquals(responseEntity.getStatusCodeValue(), 401);
        assertNull(responseEntity.getBody());
    }
}
