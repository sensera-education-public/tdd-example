package com.example.demo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SpringBootTest
class AuthServiceTest {

    @Autowired AuthService authService;
    @MockBean TokenRepository tokenRepository;
    @MockBean UserRepository userRepository;
    @MockBean PasswordVerifier passwordVerifier;
    String userToken;

    @BeforeEach
    void setUp() {
        userToken = UUID.randomUUID().toString();
    }

    @Test
    void test_login_success() throws AuthenticationFailedException {
        // Given
        when(tokenRepository.createToken()).thenReturn(userToken);
        when(userRepository.findUserByUserName(anyString())).thenReturn(Optional.of(new User("", new byte[0])));
        when(passwordVerifier.verifyPassword(anyString(), any())).thenReturn(true);

        // When
        String token = authService.login("Arne", "Lösen");

        // Then
        assertEquals(userToken, token);
    }

    @Test
    void test_login_fail_because_username_not_found() {
        // Given
        when(tokenRepository.createToken()).thenReturn(userToken);
        when(userRepository.findUserByUserName(anyString())).thenReturn(Optional.empty());

        // When
        AuthenticationFailedException authenticationFailedException = assertThrows(AuthenticationFailedException.class, () ->
                authService.login("Arne", "Lösen"));

        // Then
        assertNotNull(authenticationFailedException);
    }

    @Test
    void test_login_fail_because_wrong_password() {
        // Given
        when(tokenRepository.createToken()).thenReturn(userToken);
        when(userRepository.findUserByUserName(anyString())).thenReturn(Optional.of(new User("",new byte[0])));
        when(passwordVerifier.verifyPassword(anyString(), any())).thenReturn(false);

        // When
        AuthenticationFailedException authenticationFailedException = assertThrows(AuthenticationFailedException.class, () ->
                authService.login("Arne", "Lösen"));

        // Then
        assertNotNull(authenticationFailedException);
    }
}
